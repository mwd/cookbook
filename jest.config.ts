// Sync object
const config = {
    verbose: true,
    // The root of your source code, typically /src
    // `<rootDir>` is a token Jest substitutes
    roots: ['<rootDir>/src'],

    transform: {
        '^.+\\.(js|jsx|mjs|cjs|ts|tsx)$': 'ts-jest',
        '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
        '^(?!.*\\.(js|jsx|mjs|cjs|ts|tsx|css|json)$)':
            '<rootDir>/config/jest/fileTransform.js',
    },
    transformIgnorePatterns: [
        '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs|cjs|ts|tsx)$',
        '^.+\\.module\\.(css|sass|scss)$',
    ],
    modulePaths: ['<rootDir>/src'],
    moduleNameMapper: {
        '\\.\\/src': '<rootDir>/src/$1',
        '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
    },
    moduleFileExtensions: ['js', 'ts', 'tsx', 'json', 'jsx', 'node', 'scss'],

    // Runs special logic, such as cleaning up components
    // when using React Testing Library and adds special
    // extended assertions to Jest
    setupFilesAfterEnv: [
        // "@testing-library/react/cleanup-after-each",
        '@testing-library/jest-dom/extend-expect',
    ],

    setupFiles: ['jest-localstorage-mock', '<rootDir>/src/setupTest.ts'],

    // Test spec file resolution pattern
    // should contain `test` or `spec`.
    resetMocks: false,
    testMatch: [
        'src/**/?(*.)+(spec|test).(ts|tsx)',
        '**/?(*.)+(spec|test).(js|ts|tsx)',
    ],
};
module.exports = config;
