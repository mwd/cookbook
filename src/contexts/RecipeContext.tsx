import { useCallback, useState } from 'react';
import * as React from 'react';
import ShortUniqueId from 'short-unique-id';
import useStoreContext from '../hooks/useStoreContext';
import { Recipe } from '../types';

export const defaultRecipeContext = {
    recipes: [],
    addRecipe: r => null,
    saveRecipe: r => null,
    getRecipe: id => null,
    deleteRecipe: id => null,
    filterRecipes: filter => null,
};

export const context = React.createContext<{
    addRecipe: (newRecipe: Omit<Recipe, 'id'>) => void;
    saveRecipe: (update: Recipe) => void;
    getRecipe: (id: Recipe['id']) => Recipe | null;
    deleteRecipe: (id: Recipe['id']) => void;
    filterRecipes: (filter: string) => void;
    filter?: string;
    recipes: Recipe[];
}>(defaultRecipeContext);

const uid = new ShortUniqueId();

export default function RecipeContext({
    children,
}: {
    children: React.ReactNode;
}) {
    const { recipes, updateStorage } = useStoreContext();
    const [filter, setFilter] = useState('');

    const addRecipe = useCallback(
        (newRecipe: Omit<Recipe, 'id'>) => {
            updateStorage({
                recipes: [...recipes, { id: uid.randomUUID(5), ...newRecipe }],
            });
        },
        [recipes]
    );

    const getRecipe = useCallback(
        id => {
            return recipes.find(item => item.id === id) || null;
        },
        [recipes]
    );

    const saveRecipe = useCallback(
        (update: Recipe) => {
            updateStorage({
                recipes: recipes.reduce(
                    (acc, item) =>
                        item.id === update.id
                            ? [...acc, update]
                            : [...acc, item],
                    []
                ),
            });
        },
        [recipes]
    );

    const deleteRecipe = useCallback(
        (id: Recipe['id']) => {
            updateStorage({
                recipes: recipes.reduce(
                    (acc, item) => (item.id === id ? acc : [...acc, item]),
                    []
                ),
            });
        },
        [recipes]
    );

    const filterRecipes = (newFilter: string) => {
        setFilter(newFilter);
    };

    return (
        <context.Provider
            value={{
                recipes: filter
                    ? recipes.filter(({ name }) =>
                          new RegExp(filter, 'ig').test(name)
                      )
                    : recipes,
                addRecipe,
                getRecipe,
                saveRecipe,
                deleteRecipe,
                filterRecipes,
            }}
        >
            {children}
        </context.Provider>
    );
}
