import * as React from 'react';
import { useCallback, useState } from 'react';
import { storageKey } from '../constants';
import { Store } from '../types';

const defaultStore = {
    user: 'new user',
    recipes: [],
};
export const defaultStoreContext = {
    ...defaultStore,
    updateStorage: u => null,
    synced: 0,
};

export const context = React.createContext<Store>(defaultStoreContext);

interface StoreProviderProps {
    children?: React.ReactNode;
}

export default function StoreContext({ children }: StoreProviderProps) {
    const [synced, setSynced] = useState(new Date().getTime());

    // init
    if (!localStorage.getItem(storageKey)) {
        localStorage.setItem(storageKey, JSON.stringify(defaultStore));
    }

    // setup
    const [storage, setStorage] = useState<Store>({
        ...defaultStore, // sanitizes store only if properties are added
        ...JSON.parse(localStorage.getItem(storageKey)),
    });

    // persist changes
    const updateStorage = useCallback(
        (update: Partial<Store>) => {
            setStorage(current => {
                const nextStorage = { ...current, ...update };
                localStorage.setItem(
                    storageKey,
                    JSON.stringify({ ...nextStorage })
                );
                setSynced(new Date().getTime());
                return nextStorage;
            });
        },
        [setStorage, setSynced]
    );

    return (
        <context.Provider value={{ ...storage, updateStorage, synced }}>
            {children}
        </context.Provider>
    );
}
