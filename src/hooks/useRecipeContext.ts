import { useContext } from 'react';
import { context } from '../contexts/RecipeContext';

export default function useRecipeContext() {
    return useContext(context);
}
