import { ChangeEvent, FormEvent, useState } from 'react';

export default function useForm<T>({
    initialData,
    onSubmit,
}: {
    initialData: T;
    onSubmit: (data: T) => void;
}) {
    const [data, setData] = useState<T>(initialData);
    const [changed, setChanged] = useState(false);

    const handleChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
        setData(state => ({
            ...state,
            [target.name]: target.value,
        }));
        setChanged(true);
    };

    const handleSubmit = (event: FormEvent) => {
        event.preventDefault();
        setChanged(false);
        if (onSubmit) onSubmit(data);
    };

    return {
        handleChange,
        changed,
        data,
        handleSubmit,
    };
}
