import { useContext } from 'react';
import { context } from '../contexts/StoreContext';
import { Store } from '../types';

export default function useStoreContext() {
    return useContext<Store>(context);
}
