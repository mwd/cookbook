// Import
import { useState } from 'react';
import ShortUniqueId from 'short-unique-id';
import { Item } from '../types';

const uid = new ShortUniqueId();

export default function useList<T = any>(init: Item<T>[]) {
    const [list, setList] = useState(init);

    const addItem = (item: T) => {
        setList(state => [...state, { uid: uid.randomUUID(5), item }]);
    };
    const removeItem = (uid: Item['uid']) => {
        setList(state =>
            state.reduce(
                (newList, item) =>
                    item.uid === uid ? newList : [...newList, item],
                []
            )
        );
    };

    return {
        list,
        addItem,
        removeItem,
    };
}
