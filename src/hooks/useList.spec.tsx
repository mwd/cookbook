import { act, render } from '@testing-library/react';
import React from 'react';
import useList from './useList';

const initialList = [
    { uid: 'kMfFi', item: 'test' },
    { uid: 'YFpmB', item: 'test' },
];

function setup(init) {
    const returnVal: any = {};
    function TestComponent() {
        Object.assign(returnVal, useList(init));
        return null;
    }
    render(<TestComponent />);
    return returnVal;
}

describe('useList', () => {
    it('should initialize a list ', function() {
        const { list } = setup(initialList);
        expect(list.length).toEqual(initialList.length);
        list.forEach(item => {
            expect(item.item).not.toBe(undefined);
        });
    });

    it('should add an item with a new uid', function() {
        const hook = setup([]);
        expect(hook.list.length).toEqual(0);

        act(() => {
            hook.addItem('test');
        });
        expect(hook.list.length).toEqual(1);
        expect(hook.list[0].uid).not.toBe(undefined);
        expect(hook.list[0].item).toEqual('test');
    });

    it('should remove an item', function() {
        const hook = setup(initialList);

        act(() => {
            hook.removeItem(initialList[0].uid);
        });
        expect(hook.list.length).toEqual(1);
        expect(hook.list[0].uid).toEqual(initialList[1].uid);
        expect(hook.list[0].item).toEqual(initialList[1].item);
    });
});
