import { act, render } from '@testing-library/react';
import React from 'react';
import RecipeContext, { defaultRecipeContext } from '../contexts/RecipeContext';
import { context } from '../contexts/StoreContext';
import { Recipe } from '../types';
import useRecipeContext from './useRecipeContext';

const initialRecipes = [
    { id: 'kMfFi', name: 'test one', description: 'desc' },
    { id: 'YFpmB', name: 'test two', description: 'desc' },
];

function setup(init?: Recipe[]) {
    const returnVal: any = {};
    function TestComponent() {
        Object.assign(returnVal, useRecipeContext());
        return null;
    }
    render(
        <context.Provider
            value={{
                user: 'test',
                updateStorage: () => null,
                recipes: init || initialRecipes,
                synced: 1,
            }}
        >
            <RecipeContext>
                <TestComponent />
            </RecipeContext>
        </context.Provider>
    );
    return returnVal;
}

describe('useRecipeContext', () => {
    it('should filter recipes', function() {
        const hook = setup();
        expect(hook.recipes.length).toEqual(2);
        act(() => {
            hook.filterRecipes('one');
        });
        expect(hook.recipes.length).toEqual(1);
        expect(hook.recipes[0].name).toContain('one');
    });
});
