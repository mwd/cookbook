import * as React from 'react';
import { useHistory } from 'react-router-dom';
import useRecipeContext from '../hooks/useRecipeContext';
import { Recipe } from '../types';
import './RecipeListItem.scss';

export default function RecipeListItem({ id, name, description }: Recipe) {
    const { deleteRecipe } = useRecipeContext();
    const { push } = useHistory();

    return (
        <section className={'recipe-list-item'}>
            <div
                className={'recipe-list-item__main'}
                onClick={() => push(`/cook/${id}`)}
                role={'button'}
                tabIndex={0}
            >
                <div className="recipe-list-item__content">
                    <h2
                        className={'recipe-list-item__title'}
                        data-testid={'name'}
                    >
                        {name}
                    </h2>
                    <p
                        className={'recipe-list-item__description'}
                        data-testid={'description'}
                    >
                        {description.length < 255
                            ? description
                            : `${description.slice(0, 255)}...`}
                    </p>
                </div>
                <button className={'button recipe-list-item__read'}>
                    cook it !
                </button>
            </div>
            <div className="recipe-list-item__meta">
                <div className="recipe-list-item__controls">
                    <button
                        className={'button button--small'}
                        onClick={() => push(`/edit/${id}`)}
                    >
                        edit
                    </button>
                    <button
                        className={'button button--small'}
                        onClick={() => deleteRecipe(id)}
                    >
                        delete
                    </button>
                </div>
            </div>
        </section>
    );
}
