import * as React from 'react';
import { useParams } from 'react-router-dom';
import useForm from '../hooks/useForm';
import useRecipeContext from '../hooks/useRecipeContext';
import Input from './Input';

const initialRecipe = {
    name: 'new recipe',
    description: 'new description',
    instruction: 'new instruction',
};

export default function RecipeForm() {
    const { id } = useParams<{ id: string }>();
    const { addRecipe, getRecipe, saveRecipe } = useRecipeContext();
    const recipe = getRecipe(id) || initialRecipe;

    const { handleChange, data, handleSubmit, changed } = useForm({
        initialData: recipe,
        onSubmit: data => (id ? saveRecipe({ id, ...data }) : addRecipe(data)),
    });

    return (
        <form onSubmit={handleSubmit}>
            <Input
                type="text"
                name={'name'}
                value={data.name}
                onChange={handleChange}
            />
            <Input
                type={'area'}
                name={'description'}
                value={data.description}
                onChange={handleChange}
            />
            <Input
                type={'area'}
                name={'instruction'}
                value={data.instruction}
                onChange={handleChange}
            />
            <button
                className={'input__submit'}
                type={'submit'}
                disabled={!changed}
            >
                save
            </button>
        </form>
    );
}
