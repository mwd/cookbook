import * as React from 'react';
import { NavLink } from 'react-router-dom';
import useRecipeContext from '../hooks/useRecipeContext';
import Input from './Input';

export default function AppMenu() {
    const { filter, filterRecipes } = useRecipeContext();

    return (
        <menu className={'app__menu'}>
            <NavLink
                exact
                to="/"
                className={'app__menu__link'}
                activeClassName="is-active"
            >
                {' '}
                recipes{' '}
            </NavLink>{' '}
            <NavLink
                to="/add"
                className={'app__menu__link'}
                activeClassName="is-active"
            >
                {' '}
                add recipe{' '}
            </NavLink>{' '}
            <Input
                className={'app__menu__search'}
                type={'text'}
                placeholder={'search in names'}
                value={filter}
                onChange={({ target: { value } }) => filterRecipes(value)}
            />
        </menu>
    );
}
