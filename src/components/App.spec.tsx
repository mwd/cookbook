import { render } from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import App from './App';

describe('app', () => {
    it('should render', function() {
        const { container } = render(<App />);
        expect(container).not.toBe(undefined);
    });
});
