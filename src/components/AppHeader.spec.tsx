import { render } from '@testing-library/react';
import React from 'react';

import AppHeader from './AppHeader';

describe('<AppHeader />', () => {
    it('should render', async () => {
        const { findByTestId } = render(<AppHeader />);
        const header = await findByTestId('app-header');
        expect(header).toBeInTheDocument();
    });
});
