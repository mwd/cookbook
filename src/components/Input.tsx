import * as React from 'react';
import './Input.scss';

export default function Input({
    type = 'text',
    name,
    value,
    label,
    className,
    ...props
}: {
    type: 'text' | 'area';
    value?: string | number;
    name?: string;
    label?: string;
    [index: string]: any;
}) {
    const controlProps = {
        id: `input-${name}`,
        name,
        value,
        ...props,
    };

    return (
        <div className={['input__control', className].join(' ')}>
            <label className={'input__label'} htmlFor={controlProps.id}>
                {label || name}
            </label>
            {type === 'text' ? (
                <input className={'input__input'} {...controlProps} />
            ) : (
                <textarea className={'input__textarea'} {...controlProps} />
            )}
        </div>
    );
}
