import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import RecipeContext from '../contexts/RecipeContext';
import RecipeDetail from './RecipeDetail';
import RecipeForm from './RecipeForm';
import RecipeList from './RecipeList';

export default function AppMain() {
    return (
        <main className={'app__main'}>
            <Switch>
                <Route exact path="/">
                    <RecipeList />
                </Route>
                <Route path={['/add', '/edit/:id']}>
                    <RecipeForm />
                </Route>
                <Route path="/cook/:id">
                    <RecipeDetail />
                </Route>
            </Switch>
        </main>
    );
}
