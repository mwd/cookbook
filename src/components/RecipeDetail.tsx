import * as React from 'react';
import { useParams } from 'react-router-dom';
import useRecipeContext from '../hooks/useRecipeContext';
import './RecipeDetail.scss';

export default function RecipeDetail() {
    const { id } = useParams<{ id: string }>();
    const { getRecipe } = useRecipeContext();
    const { name, description, instruction } = getRecipe(id);

    return (
        <>
            <h2 className={'recipe-detail__title'}>{name}</h2>
            <h3 className={'recipe-detail__description'}>{description}</h3>
            <ul className={'recipe-detail__instruction'}>
                {instruction.split('\n').map(part => (
                    <li key={part}>{part}</li>
                ))}
            </ul>
        </>
    );
}
