import * as React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import RecipeContext from '../contexts/RecipeContext';
import StoreContext from '../contexts/StoreContext';
import './App.scss';
import AppFooter from './AppFooter';
import AppHeader from './AppHeader';
import AppMain from './AppMain';
import AppMenu from './AppMenu';

export default function App() {
    return (
        <React.Suspense fallback={'loading mwd-app ...'}>
            <Router
                basename={
                    location.hostname.includes('gitlab') ? 'cookbook/' : '/'
                }
            >
                <div className={'app__layout'}>
                    <StoreContext>
                        <RecipeContext>
                            <AppHeader />
                            <AppMenu />
                            <AppMain />
                            <AppFooter />
                        </RecipeContext>
                    </StoreContext>
                </div>
            </Router>
        </React.Suspense>
    );
}
