import * as React from 'react';
import MwdLogo from '../icons/MwdLogo';

export default function AppHeader() {
    return (
        <header data-testid={'app-header'} className={'app__header'}>
            <div>
                <div className={'app__header__title'}>Cookbook</div>
            </div>
            <MwdLogo className={'app__header__logo'} />
        </header>
    );
}
