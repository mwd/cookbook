import * as React from 'react';
import useRecipeContext from '../hooks/useRecipeContext';
import RecipeListItem from './RecipeListItem';

export default function RecipeList() {
    const { recipes } = useRecipeContext();

    return (
        <>
            {recipes.length ? (
                recipes.map(recipe => (
                    <RecipeListItem key={recipe.id} {...recipe} />
                ))
            ) : (
                <p>no recipes yet</p>
            )}
        </>
    );
}
