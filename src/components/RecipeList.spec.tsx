import { render } from '@testing-library/react';
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { context, defaultRecipeContext } from '../contexts/RecipeContext';
import RecipeList from './RecipeList';

describe('<RecipeList /> with provider', () => {
    it('should render', async () => {
        const { findByTestId } = render(
            <Router>
                <context.Provider
                    value={{
                        ...defaultRecipeContext,
                        recipes: [
                            {
                                id: '1',
                                name: 'name',
                                description: 'desc',
                            },
                        ],
                    }}
                >
                    <RecipeList />
                </context.Provider>
            </Router>
        );
        const name = await findByTestId('name');
        const description = await findByTestId('description');
        expect(name).toBeInTheDocument();
        expect(description).toBeInTheDocument();
    });
});
