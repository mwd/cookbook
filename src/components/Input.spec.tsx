import { act, fireEvent, render } from '@testing-library/react';
import React from 'react';
import Input from './Input';

const onChange = jest.fn();

describe.only('Input', () => {
    it('should render as type input', () => {
        const { container } = render(<Input type={'text'} />);
        expect(container.querySelector('input')).toBeInTheDocument();
    });

    it('should render as type textarea', () => {
        const { container } = render(<Input type={'area'} />);
        expect(container.querySelector('textarea')).toBeInTheDocument();
    });

    it('should emit its value on change', () => {
        const { container } = render(
            <Input type={'text'} onChange={onChange} />
        );

        onChange.mockReturnValue('value');
        fireEvent.change(container.querySelector('input'), {
            target: { value: 'value' },
        });

        expect(onChange.mock.results[0].value).toEqual('value');
    });
});
