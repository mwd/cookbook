import * as React from 'react';
import { CSSProperties } from 'react';

const style: { [index: string]: CSSProperties } = {
    transparent: { fill: 'none', stroke: '#000000', strokeMiterlimit: 10 },
    blue: { fill: '#1463CC', stroke: '#000000', strokeMiterlimit: 10 },
    white: { fill: '#FFFFFF', stroke: '#000000', strokeMiterlimit: 10 },
};

export default function MwdLogo(props: any) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            viewBox="0 0 200 174"
            xmlSpace="preserve"
            {...props}
        >
            <g>
                <polygon
                    style={style.transparent}
                    points="3,86.9 51.5,3 148.5,3 172.8,45 124.2,45 100,86.9 100,86.9 99.9,86.9 51.5,86.9 27.2,129 	"
                />
                <polyline
                    style={style.blue}
                    points="124.2,129 100,171 100,171 100,171 148.5,171 172.8,128.9 172.8,128.9 172.8,129 124.2,129 	"
                />
                <polygon
                    style={style.white}
                    points="99.9,86.9 51.5,86.9 27.2,129 51.5,171 100,171 75.8,128.9 	"
                />
                <polygon
                    style={style.white}
                    points="172.8,128.9 197,86.9 172.8,45 124.2,45 100,86.9 148.5,86.9 	"
                />
            </g>
        </svg>
    );
}
