export interface Ingredient {
    name: string;
    url: string[];
}

export interface Recipe {
    id: string;
    name: string;
    description: string;
    ingredients?: string[];
    instruction?: string;
}

export interface Item<T = any> {
    uid: string;
    item: T;
}

export interface Store {
    user: string;
    recipes: Recipe[];
    updateStorage: (update: Partial<Storage>) => void;
    synced: number;
}

export interface StoreContext extends Store {
    updateStorage;
    synced;
}
