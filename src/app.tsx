import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'typeface-spartan';
import './Common.scss';
import App from './components/App';

ReactDOM.render(<App />, document.getElementById('app'));
